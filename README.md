# Introducing Accelerating Cybersecurity with Artificial Intelligence (ACAI) - A collection of Notebooks

Welcome to Epigen [Technology Corp's](https://www.epigentechnology.com) notebook repo for Accelerating Cyber with Artificial Intelligence (ACAI)!

This repo contains notebooks from various cyber security conferences at which we attend and present. We know that not everyone can attend all of the conferences. With that in mind, 
we do not want people to miss out. In the spirit of open communities and continuing education, we are offering our notebooks to the public.


The purpose of this collection of notebooks is to help users understand the techniques of proctical applications of AI in cyber, learn why, how, and when applying AI and the data
science pipeline makes sense.

All of of these notebooks use GPUs for accelerating training and inference as well as libraries from the PyData ecosystem, RAPIDS from NVIDIA, and additinal Python packages, 
and may include code for downloading datasets - therefore, they require network connectivity.

We will also provide instructions for setting up a GPU accelerated environment in various cloud provider environments.

If you are here through our InfoSec World class and have any questions, please email us at infosecai@epigentechnology.com

# Exploring the Repo

- `install_instructions`:this directory is where you will find the install instructions for the various cloud service providers
- `notebooks`: in this directory you will find each of our notebooks with their associated image and data files
- `docker`: this directory houses the docker command for installing the image needed to run the notebooks

# The Notebooks

| Notebook Name | Notebook Location | Description |
| -----  | ----- | ----- |
| Flow Tutorial | [/notebooks/flow_tutorial/](https://gitlab.com/EpigenTechnologyCorp1/acai/-/tree/master/notebooks/flow_tutorial) | Using XGBoost to predict network device types |
| GraphLink Tutorial | [/notebooks/graphlink_tutorial/](https://gitlab.com/EpigenTechnologyCorp1/acai/-/tree/master/notebooks/graphlink_tutorial) | Using PageRank to detect and visualize botnet traffic |


