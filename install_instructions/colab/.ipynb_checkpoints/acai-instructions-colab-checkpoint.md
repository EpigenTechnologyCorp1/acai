# Accelerating Cybersecurity with Artificial Intelligence:

## Instructions to deploy GCP (Google Cloud) environment
-------------

These instructions are prepared to set the environment for the INFOSEC WORLD 2020 "Accelerated Cybersecuirity using AI (ACAI) tutorial".

## Prerequisites:
------------

### At minimum you just need Google account. 

Without Google account login you will be able to see the notebooks, but you will not be able to execute the code. If you have Gmail, you have Google account. You can create Google account <a href="https://support.google.com/accounts/answer/27441?hl=en">here</a>.

### Google Colab.

Google Colab is free online machine learning platform that provided online notebooks with GPUs support. 

### GPU requirement for the tutorial notebooks>

To successfully run the notebooks for ACAI course you will need runtime with P4, T4 or P100 GPU type.

When you start a notebook you may get a K80 GPU which is not supported by Rapids. You will have to restart the runtime per below instructions.

### Colab Pro option.

Google Colab have a payed option Colab Pro for $10 per month which will get you P100 GPUs all the time.
It is up to you to decide wheather to try free option or to switch to the paid option.

--------------------------

## ACAI Notebooks

### The first tutorial notebook. 

https://colab.research.google.com/drive/1JBLJn6VesD4Y3S_CI4VQc0bN0LpmPNr7?usp=sharing


### The second tutorial notebook. 

https://colab.research.google.com/drive/1ExK8Xw7AlZe6dYlytH9c_2M_F0YLV-ua?usp=sharing

--------------------

## Instructions and useful tips.

1. To open a notebook just click on links above. After opening a notebook by click the above links, make a copy of the notebook to able to run the code.

<img src="img/img1.png" alt="" style="width: 70%;"/>
<br>

2. When the notebook is active check the GPU type by running the first cell. Note the GPU type in the output. If it is P100 - this is the best. Notebooks also should work with P4 and T4 NVIDIA GPUs.

<img src="img/img2.png" alt="" style="width: 70%;"/>
<br>

If you got "K80" then you need to switch the GPU type. This is done by the "Factory reset" the runtime by clicking "Runtime" -> "Factory Rest". Confirm by clicking "YES" as shown below.

<img src="img/img3.png" alt="" style="width: 70%;"/>
<img src="img/img4.png" alt="" style="width: 70%;"/>
<br>

When you get the right GPU type you are ready to start the tutorial. Good luck!