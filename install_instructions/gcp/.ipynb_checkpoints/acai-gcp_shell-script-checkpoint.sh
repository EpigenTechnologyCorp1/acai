sudo docker pull rapidsai/rapidsai:cuda10.0-runtime-ubuntu18.04-py3.6
cd acai/docker
sudo docker build -t acai:v1 .
cd 
sudo docker run --gpus all --rm -it -p 8888:8888 -p 8787:8787 -p 8786:8786 -v `pwd`/acai:/rapids/notebooks/acai acai:v1
