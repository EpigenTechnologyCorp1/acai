# Accelerating Cybersecurity with Artificial Intelligence:

## Instructions to deploy GCP (Google Cloud) environment
-------------

These instructions are prepared to set the environment for the INFOSEC WORLD 2020 "Accelerated Cybersecuirity using AI (ACAI) tutorial".

## Prerequisites:
------------

### You need Google Cloud account. 

<span style="color:red">
Be advised that this instruction will deploy a GPU instance in the Google cloud and by default you will be billed for using this resource. Please, make sure to delete the resources created for the course after you done with the tutorial to prevent excessive billing.  The tutorial environment uses V100 GPU.</span>

-------------

#### 1. Login to Google Cloud console. Create a new project by clicking on "Select Project", then onto "Mew Project"

<img src="img/instr1.png" alt="" style="width: 70%;"/>
<img src="img/instr1_1.png" alt="" style="width: 70%;"/>
<br>

#### 2. Enter the name for the project. We used "ACAI", but it can be any name. Click "Create".

<img src="img/instr2.png" alt="" style="width: 70%;"/>
<br>

#### 3. In the Google Cloud menu, clikc "Marketplace".

<img src="img/instr3.png" alt="" style="width: 70%;"/>
<br>

#### 4. In the search window enter `deep`. The dropdown menu should appear. Click on "NVIDIA GPU Cloud Image for Deep Learning ... "

<img src="img/instr4.png" alt="" style="width: 70%;"/>
<br>

#### 5. On the Image page, click "Launch".

<img src="img/instr5.png" alt="" style="width: 70%;"/>
<br>

#### 6. Wait for configuration process to complete.

<img src="img/instr6.png" alt="" style="width: 70%;"/>
<br>

#### 7. You should see the following screen. By default the GPUs quotas are set to zero. We need to request the GPU quotas to be increased. Unfortunately, this practice common to all GPU cloud providers, so one can't bypass this step. So, click on "quotas page"

<img src="img/instr7.png" alt="" style="width: 70%;"/>
<br>

#### 8. Click "Edit Quotas".

<img src="img/instr8.png" alt="" style="width: 70%;"/>
<br>

#### 9. Select the "GPUs (all regions)" quota in the list (should be the only one available to choose). Enter you contact information. Click "MEXT".

<img src="img/instr9.png" alt="" style="width: 70%;"/>
<br>

#### 10. For the requested quote enter 1 (should be enough), and enter short explanation. Click "Submit request"

  
<img src="img/instr10.png" alt="" style="width: 70%;"/>
<br>

#### 11. You should see confirmation screen. The approval process should not take too much time. For us, the last time we had to do this, the approval email arrived in 3 minutes. 
    
<img src="img/instr11.png" alt="" style="width: 70%;"/>
<br>



----------------------
<br>
<br>

# Tear Down Instructions
-------------------------
<span style="color:red">
To avoid extra costs, destroy the environment as soon as you are finished with the course.
</span>

