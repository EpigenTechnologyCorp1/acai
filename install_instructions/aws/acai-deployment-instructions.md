# Accelerating Cybersecurity with Artificial Intelligence:

## Instructions to deploy AWS environment
-------------

These instructions are prepared to set the environment for the INFOSEC WORLD 2020 "Accelerated Cybersecuirity using AI (ACAI) tutorial".

## Prerequisites:
------------

### You need AWS account. 

<span style="color:red">
Be advised that this instruction will deploy a GPU instance in the AWS cloud and by defauld you will be billed for using this resource. Please, make sure to delete the AWS CloudFormation stack or resources after you done with the tutorial to prevent excessive billing. Make sure your AWS account EC2 limits do not restrict the number of vCPUs to be less than 8. The tutorial environment uses p3.2xlarge instances which have 8 vCPUs. These instructions will work only in us-east-1 region.</span>

-------------

#### 1. Login to AWS console. Make sure you set the AWS region to *us-east-1*

<img src="img/instr1.png" alt="" style="width: 70%;"/>
<img src="img/instr1_1.png" alt="" style="width: 70%;"/>
<br>

#### 2. Go to Cloud Formation

<img src="img/instr2.png" alt="" style="width: 70%;"/>
<br>

#### 3. Click *Create Stack* > *With new resources*

<img src="img/instr3.png" alt="" style="width: 70%;"/>
<br>

#### 4. Enter `https://acai-tutorial.s3.amazonaws.com/acai.template` for S3 URL, click *Next*

<img src="img/instr4.png" alt="" style="width: 70%;"/>
<br>

#### 5. Enter the name for the stack. We use "ACAI", but you can use any name you like. Choose your Jupyter login password. Keep the password handy and secure. You will need it to log into the tutorial environment.  Click *Next*

<img src="img/instr5.png" alt="" style="width: 70%;"/>
<br>

#### 6. At the *Create Stack Options* page, just click *Next*

<img src="img/instr6.png" alt="" style="width: 70%;"/>
<br>

#### 7. Finally, click *Create*

<img src="img/instr7.png" alt="" style="width: 70%;"/>
<br>

#### 8. You will see stack creation in progress

<img src="img/instr8.png" alt="" style="width: 70%;"/>
<br>

#### 9. After stack creation is complete, click on the *Outputs* tab and click on the Jupyter Lab URL 

<img src="img/instr9.png" alt="" style="width: 70%;"/>
<img src="img/instr10_1.png" alt="" style="width: 70%;"/>
<br>

#### 10. You should see the Jupyter Lab login screen. 

```
NOTE: Be advised that there may be a 1-2 minute delay between the moment the stack is created and the moment the endpoint is available as there a few more configuration steps have to finalized. Please be patient... 
```    
<img src="img/instr11.png" alt="" style="width: 70%;"/>
<br>

#### 11. Enter the password you set during the step 5 above, you should see the screen below. Navigate to `acai/notebooks/flow_tutorial` directory.
    
<img src="img/instr12_1.png" alt="" style="width: 70%;"/>
<img src="img/instr12_2.png" alt="" style="width: 70%;"/>
<img src="img/instr12_3.png" alt="" style="width: 70%;"/>
<br>

#### 12. Double click on *acai_device_classification.ipynb*. Notebook tab should open. Congratulations! You are ready to start the first tutorial!

<img src="img/instr13_1.png" alt="" style="width: 70%;"/>
<br>

----------------------
<br>
<br>

# Tear Down Instructions
-------------------------
<span style="color:red">
To avoid extra costs, destroy the environment as soon as you are finished with the course.
</span>

#### 1. On the stack page, click "Delete"
<img src="img/instr14_1.png" alt="" style="width: 70%;"/>
<br>

#### 2. Then confirm by clicking "Delete stack"
<img src="img/instr14_2.png" alt="" style="width: 70%;"/>
<br>