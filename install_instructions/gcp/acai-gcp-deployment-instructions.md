# Accelerating Cybersecurity with Artificial Intelligence:

## Instructions to deploy GCP (Google Cloud) environment
-------------

These instructions are prepared to set the environment for the INFOSEC WORLD 2020 "Accelerated Cybersecuirity using AI (ACAI) tutorial".

## Prerequisites:
------------

### You need Google Cloud account. 

<span style="color:red">
Be advised that this instruction will deploy a GPU instance in the Google cloud and by default you will be billed for using this resource. Please, make sure to delete the resources created for the course after you done with the tutorial to prevent excessive billing.  The tutorial environment uses Tesla V100 GPU instances. These instances available on the paid account. If you have a free tier account you will need to upgrade to paid account per <a href='https://cloud.google.com/free/docs/gcp-free-tier#how-to-upgrade'>these instructions</a>. If you have free credit will still be applied. </span>

-------------

#### 1. Login to Google Cloud console. Create a new project by clicking on "Select Project", then onto "New Project"

<img src="img/instr1.png" alt="" style="width: 70%;"/>
<img src="img/instr1_1.png" alt="" style="width: 70%;"/>
<br>

#### 2. Enter the name for the project. We used "ACAI", but it can be any name. Click "Create".

<img src="img/instr2.png" alt="" style="width: 70%;"/>
<br>

#### 3. In the Google Cloud menu, clikc "Marketplace".

<img src="img/instr3.png" alt="" style="width: 70%;"/>
<br>

#### 4. In the search window enter `deep`. The dropdown menu should appear. Click on "NVIDIA GPU Cloud Image for Deep Learning ... "

<img src="img/instr4.png" alt="" style="width: 70%;"/>
<br>

#### 5. On the Image page, click "Launch".

<img src="img/instr5.png" alt="" style="width: 70%;"/>
<br>

#### 6. Wait for configuration process to complete.

<img src="img/instr6.png" alt="" style="width: 70%;"/>
<br>

#### 7. You should see the following screen. By default the GPUs quotas are set to zero. We need to request the GPU quotas to be increased. Unfortunately, this practice common to all GPU cloud providers, so one can't bypass this step. So, click on "quotas page"

<img src="img/instr7.png" alt="" style="width: 70%;"/>
<br>

#### 8. Click "Edit Quotas".

<img src="img/instr8.png" alt="" style="width: 70%;"/>
<br>

#### 9. Select the "GPUs (all regions)" quota in the list (should be the only one available to choose). Enter you contact information. Click "MEXT".

<img src="img/instr9.png" alt="" style="width: 70%;"/>
<br>

#### 10. For the requested quote enter 1 (should be enough), and enter short explanation. Click "Submit request"

  
<img src="img/instr10.png" alt="" style="width: 70%;"/>
<br>

#### 11. You should see confirmation screen. The approval process should not take too much time. For us, the last time we had to do this, the approval email arrived in 3 minutes. 
    
<img src="img/instr11.png" alt="" style="width: 70%;"/>
<br>

#### 12. After the GPU quote increase you should be able to launch the instance by clicking on "Deploy" button. Double check that you have "NVIDIA Tesle V100" selected for GPU type. 
    
<img src="img/instr12.png" alt="" style="width: 70%;"/>
<br>

#### 13. There is a good chance that the instance fail to deploy due to resource being unavailable in the selected region.This is most probably due to all GPUs being busy in this region. The only way to overcome this is try different regions by going back to deployment page and selecting various regions where V100s are actually available and the image is able to deploy.  
    
<img src="img/instr13.png" alt="" style="width: 70%;"/>
<br>

---------------

#### 14. You should see the message saying that the instance had being created. Login into the machine by clicking to "SSH" button on the right.


<img src="img/instr14.png" alt="" style="width: 70%;"/>
<br>


---------------

#### 15. The shell window will open. Enter 'Y' for the prompt asking if you want to update NVIDIA drivers.

<img src="img/instr15.png" alt="" style="width: 70%;"/>
<br>
<br>

---------------

#### 16. When the shell prompt appears, enter the following commands:

`
$ git clone https://gitlab.com/EpigenTechnologyCorp1/acai.git
`


<img src="img/instr16.png" alt="" style="width: 70%;"/>
<br>
<br>


`
$ bash acai/install_instructions/gcp/acai-gcp_shell-script.sh
`


<img src="img/instr17.png" alt="" style="width: 70%;"/>
<br>
<br>

----------------------

#### 17. The last thing left to do is to open a port for Jupyter Lab. This is done by going to VPC network -> Firewall and clicking the " + Create a firewall rule"

<img src="img/instr18.png" alt="" style="width: 70%;"/>
<br>
<br>


----------------------

#### 18. Select "All instances in network" for Targets, enter "0.0.0.0/0" for source IPs, check the tcp checkbox and enter 8888 for the inbound port next to the tcp checkbox.

<img src="img/instr19.png" alt="" style="width: 70%;"/>
<img src="img/instr20.png" alt="" style="width: 70%;"/>

<br>
<br>

-----------

#### 19. After the port is open, the environment is available. Find the external IP of the instance on the instance page (Compute Engine -> VM Instances -> click on the instance), copy it. 

<img src="img/instr21.png" alt="" style="width: 70%;"/>

<br>
<br>

--------------

#### 20. Paste the IP address of the instance to the browser address window, add ":8888/lab?" and press Enter.
<img src="img/instr22.png" alt="" style="width: 70%;"/>
<br>
<br>

------------

#### 21. Jupyter Lab login page should load. Enter 'infosec' for password. 
<img src="img/instr23.png" alt="" style="width: 70%;"/>
<br>
<br>

------------

#### 22. Go to `acai/notebooks/flow_tutorial`. 
<img src="img/instr12_1.png" alt="" style="width: 70%;"/>
<img src="img/instr12_2.png" alt="" style="width: 70%;"/>
<img src="img/instr12_3.png" alt="" style="width: 70%;"/>
<br>
<br>

#### 23. Double click on `acai-flow-tutorial.ipynb`. Congratulations! You are ready to start the first tutorial.

-----------------

# Tear Down Instructions
-------------------------
<span style="color:red">
To avoid extra costs, destroy the environment as soon as you are finished with the course.
</span>

#### 21. On the VM instance details page click "Delete". Confirm by click "Delete" one more time. 
<img src="img/instr24.png" alt="" style="width: 70%;"/>
<br>
<br>
