# Instructions to deploy AWS environment

## Prerequisites.

1. AWS account

 ```
Be advised that this instruction will deploy a GPU instance in the AWS cloud and by defauld you will be billed for using this resource. Please, make sure to delete the AWS CloudFormation stack or resources after you done with the tutorial to prevent excessive billing. 
 ```

## Using AWS Console

#### 1. Login to AWS console

<img src="img/instr1.png" alt="" style="width: 70%;"/>

#### 2. Go to Cloud Formation

<img src="img/instr2.png" alt="" style="width: 70%;"/>

#### 3. Click *Create Stack* > *With new resources*

<img src="img/instr3.png" alt="" style="width: 70%;"/>

#### 4. Enter `https://acai-tutorial.s3.amazonaws.com/acai.template` for S3 URL, click *Next*

<img src="img/instr4.png" alt="" style="width: 70%;"/>

